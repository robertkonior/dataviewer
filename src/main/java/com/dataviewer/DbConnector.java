package com.dataviewer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbConnector {

    private Connection connection;
    private static DbConnector dbConnectorInstance;

    private DbConnector() throws SQLException {
        Properties connectionProps = new Properties();
        connectionProps.put("user", "postgres");
        connectionProps.put("password", "connectme");
        connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/serie", connectionProps);
    }

    public static DbConnector getInstance() throws SQLException {
        if (dbConnectorInstance == null) {
            dbConnectorInstance = new DbConnector();
        }
        return dbConnectorInstance;
    }

    public Connection getConnection() {
        return connection;
    }
}
