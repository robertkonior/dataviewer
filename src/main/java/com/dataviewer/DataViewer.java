package com.dataviewer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Vector;

public class DataViewer extends JPanel {

    private JTable table;

    private DataViewer() {
        table = new JTable(getTableModel());
        table.setPreferredScrollableViewportSize(new Dimension(1200, 500));
        table.setFillsViewportHeight(true);
        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane);
    }

    private static DefaultTableModel getTableModel() {

        DefaultTableModel dtm = new DefaultTableModel();
        try {
            DbConnector dbConnector = DbConnector.getInstance();
            Statement statement = dbConnector.getConnection().createStatement();
            String query = "SELECT * FROM public.serie " +
                    "ORDER BY id ASC ";
            ResultSet rs = statement.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();

            int column = rsmd.getColumnCount();
            Vector colName = new Vector();
            Vector dataRow ;

            for (int i = 1; i <= column; i++) {
                colName.addElement(rsmd.getColumnName(i));
            }
            dtm.setColumnIdentifiers(colName);
            while (rs.next()) {
                dataRow = new Vector();
                for (int j = 1; j <= column; j++) {
                    dataRow.addElement(rs.getString(j));
                }
                dtm.addRow(dataRow);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return dtm;
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Table Serie");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        DataViewer printer = new DataViewer();
        printer.setOpaque(true);

        frame.setContentPane(printer);
        frame.pack();
        frame.setVisible(true);
    }
}
