package com.dataviewer;

import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.*;

public class DbConnectorTest {

    @Test
    public void testConnection() throws SQLException {
        //Given
        //When
        DbConnector dbConnector = DbConnector.getInstance();
        //Then
        Assert.assertNotNull(dbConnector.getConnection());
    }
}