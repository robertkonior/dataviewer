package com.htmlviewer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HtmlviewerApplication {

    public static void main(String[] args) {
        SpringApplication.run(HtmlviewerApplication.class, args);
    }
}
