package com.htmlviewer.repository;

import com.htmlviewer.model.SerieRow;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SerieRepository extends CrudRepository<SerieRow, Long> {

    @Override
    List<SerieRow> findAll();
}
