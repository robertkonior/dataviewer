package com.htmlviewer.controller;

import com.htmlviewer.model.SerieRow;
import com.htmlviewer.repository.SerieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class SerieController {

    @Autowired
    private SerieRepository repository;

    @RequestMapping("/")
    public String getSerieTable(Model model){
        List<SerieRow> serieRowList = repository.findAll();
        model.addAttribute("serieRowList",serieRowList);
        return "table";
    }
}
