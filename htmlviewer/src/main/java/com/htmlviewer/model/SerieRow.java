package com.htmlviewer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;
import java.sql.Timestamp;

@Entity(name = "serie" )
public class SerieRow {
    @Id
    private Long id;
    @Column(name = "name")
    private String name;
    @Column (name = "file")
    private String file;
    @Column(name = "cassid")
    private String cassId;
    @Column(name = "categoryid")
    private Long categoryId;
    @Column(name = "datefrom")
    private Date dateFrom;
    @Column(name = "dateto")
    private Date  dateTo;
    @Column(name = "frequency")
    private Integer frequency;
    @Column(name = "markers")
    private Long[] markers;
    @Column(name = "unit")
    private String unit;
    @Column(name = "feed")
    private String feed;
    @Column(name = "userid")
    private Long userId;
    @Column(name = "createdate")
    private Timestamp createDate;
    @Column(name = "changedate")
    private Timestamp changeDate;

    public SerieRow() {
    }

    public SerieRow(Long id, String name, String file, String cassId,
                    Long categoryId, Date dateFrom, Date dateTo,
                    Integer frequency, Long[] markers, String unit, String feed,
                    Long userId, Timestamp createDate, Timestamp changeDate) {
        this.id = id;
        this.name = name;
        this.file = file;
        this.cassId = cassId;
        this.categoryId = categoryId;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.frequency = frequency;
        this.markers = markers;
        this.unit = unit;
        this.feed = feed;
        this.userId = userId;
        this.createDate = createDate;
        this.changeDate = changeDate;
    }


    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFile() {
        return file;
    }

    public String getCassId() {
        return cassId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public Long[] getMarkers() {
        return markers;
    }

    public String getUnit() {
        return unit;
    }

    public String getFeed() {
        return feed;
    }

    public Long getUserId() {
        return userId;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public Timestamp getChangeDate() {
        return changeDate;
    }
}
